package com.velocity.admin.ecommerce;

/* Author name - Raghvendr mane & Sanket Dhomase & Vijay Chavan & Shubham Chambhare
 * 
 */
import java.sql.Connection;
import java.sql.PreparedStatement;

public class TruncateCart implements Refresh {

	@Override
	public void getRefershCart() {
		try {
			Database db = new Database();
			Connection connection = db.getConDetails();
			PreparedStatement preparedStatement1 = connection.prepareStatement("truncate cart");
			preparedStatement1.execute();// truncate cammand cart table
		} catch (Exception e) {
			e.getMessage();
		}

	}

}
