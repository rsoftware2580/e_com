package com.velocity.admin.ecommerce;

public interface Cart {

	public void getCartDetailes(int code, int quantity);

	public void getBill();

	public void addMoreProduct();
}
