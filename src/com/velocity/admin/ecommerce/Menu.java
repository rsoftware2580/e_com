package com.velocity.admin.ecommerce;

public interface Menu {

	public void getListDetails();

	public void getProductPurches();

	public void getLoginDetails(String name, String pass);

	public void getScannerClass();
}
