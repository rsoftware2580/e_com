package com.velocity.admin.ecommerce;

import java.sql.Connection;

public interface DbConnection {

	public Connection getConDetails();
}
